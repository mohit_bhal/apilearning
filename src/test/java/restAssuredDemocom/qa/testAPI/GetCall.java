package restAssuredDemocom.qa.testAPI;

import java.io.IOException;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;




import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.qa.POJO.AddJoke;
import com.qa.POJO.FlagValues;
import com.qa.POJO.PojoGet;
import com.qa.TestBase.TestBase;

import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import io.restassured.specification.ResponseSpecification;

public class GetCall extends TestBase {
	public TestBase test;
	public Response response;
	public RequestSpecification req;
	public ResponseSpecification respspec;
	public RestAssured rest;
	String url;
	String puturl;
	String geturl;
	String finalurl;
	
	@BeforeMethod
	public void setUp() {
		test=new TestBase();
		url=prop.getProperty("URL");
		puturl=prop.getProperty("serviceURL");
		geturl=prop.getProperty("serviceURLGet");
		finalurl=url+puturl;
	}
	@Test
	public void putData() throws JsonProcessingException {
		rest.baseURI=url;
	    req=rest.given();
		FlagValues flagvalue=new FlagValues();
		flagvalue.setNsfw(true);
		flagvalue.setPolitical(true);
		flagvalue.setRacist(true);
		flagvalue.setReligious(true);
		flagvalue.setSexist(true);
		
		AddJoke addjoke=new AddJoke();
		addjoke.setFormatVersion(2);
		addjoke.setCategory(prop.getProperty("category"));
		addjoke.setType(prop.getProperty("type"));
		addjoke.setJoke(prop.getProperty("joke"));
		addjoke.setFlags(flagvalue);
		
	  
	    
		
	    ObjectMapper objMapper = new ObjectMapper();
		String json = objMapper.writeValueAsString(addjoke);
		req.body(json);
		
		Response response = req.put(puturl);
		System.out.println(response.body().asString());
		System.out.println(response.getStatusCode());
		

	}

	@Test
	public void getData() {
		
		RestAssured.baseURI=url;
		Response response=null;
		try {
			response=RestAssured.given().get(geturl);
		} catch (Exception e) {
			
			e.printStackTrace();
		}
		System.out.println("Response is "+ response.asString());
		System.out.println("Status code is " + response.statusCode());
		
		
		
	}
	@Test
	public void getDataFromPojo() throws IOException {
		RestAssured.baseURI=url;
		RequestSpecification reqs = RestAssured.given();
		Response response = reqs.request("GET",prop.getProperty("serviceURLGetCatogery"));
		ObjectMapper objectmapper = new ObjectMapper();
		 
		
			PojoGet getPojo = objectmapper.readValue(response.asString(),PojoGet.class);
			System.out.println("Get Type value is " +getPojo.getType());
		
		

	}
	

}
