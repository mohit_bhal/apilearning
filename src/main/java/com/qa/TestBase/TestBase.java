package com.qa.TestBase;

import java.io.FileInputStream;
import java.util.Properties;

public class TestBase {
	public static Properties prop;

	public  TestBase() {
		
		try {
			prop = new Properties();
			FileInputStream file = new FileInputStream(
					"/Users/mohbhal/eclipse-workspace/restAssuredDemo/src/main/java/com/qa/properties/config.properties");
			prop.load(file);

		} catch (Exception e) {
			System.out.println(e);
		}
	}

}
